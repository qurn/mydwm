/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>
/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "dejavu:size=13" }; /* tower 10 */
static const char dmenufont[]       = "monospace:size=13";
static const char sol_base00[]      = "#657b83"; /*was #bbbbbb, bright grey*/
static const char sol_cybri[]       = "#2aa198"; /*was #eeeeee, white*/
static const char sol_base03[]      = "#002b36"; /*Solarized dark background, was "#005577"*/
static const char sol_greybg[]      = "#073642";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { sol_base00, sol_greybg, sol_greybg },/*sol_base00, sol_base03, col_gray2*/
	[SchemeSel]  = { sol_cybri,  sol_base03, sol_cybri  },/*was sol_cybri, sol_base03, sol_base03*/
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class          instance  title         tags mask     isfloating   monitor */
	{ "Gimp",         NULL,     NULL,         0,            1,           -1 },
	{ "Figure.*",     NULL,     NULL,         5,            0,           -1 },
	{ "Pidgin",       "Pidgin", "Buddy List", 1 << 8,       0,           -1 },
	/*{ "broken", NULL,     NULL,         1 << 7,       1,           -1 }, */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "┃ ┣━┫",    tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },/*useless*/
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|Mod1Mask,              KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]    = { "/home/ffm/.config/myscripts/dmenu_recent.sh", "-m", dmenumon, "-fn", dmenufont, "-nb", sol_base03, "-nf", sol_base00, "-sb", sol_base03, "-sf", sol_cybri, NULL };
//static const char *dmenucmd[]    = { "j4-dmenu-desktop", "--usage-log=~/.dmenulist", "--display-binary", "-m", dmenumon, "-fn", dmenufont, "-nb", sol_base03, "-nf", sol_base00, "-sb", sol_base03, "-sf", sol_cybri, NULL };
//static const char *dmenucmd[]    = { "j4-dmenu-desktop", "-m", dmenumon, "-fn", dmenufont, "-nb", sol_base03, "-nf", sol_base00, "-sb", sol_base03, "-sf", sol_cybri, NULL };
static const char *termcmd[]     = { "urxvt", NULL };
static const char *termrgcmd[]   = { "urxvt", "-e", "sh", "-c", "/home/ffm/build/dotfiles/dotconfig/myscripts/ranger-cd.sh; bash" };
static const char *firefoxcmd[]  = { "firefox", NULL };
static const char *killfirefoxcmd[]  = { "killall", "-9", "firefox", NULL };
static const char *killcursorcmd[]  = { "killall", "-9", "cursor_indicator", NULL };
static const char *chromcmd[]    = { "chromium", "-incognito", NULL };
static const char *pidgincmd[]   = { "pidgin", NULL };
static const char *nemocmd[]     = { "nemo", NULL };
static const char *freecadcmd[]  = { "/home/ffm/build/FreeCAD/bin/FreeCAD", NULL };
static const char *poweroffcmd[] = { "tsp", "poweroff", NULL };
static const char *rebootcmd[]   = { "reboot", NULL };
static const char *suspendcmd[]  = { "pm-suspend", NULL };
static const char *lockcmd[]     = { "slock", NULL };
static const char *appcmd[]      = { "xfce4-appfinder", NULL };
static const char *clipcmd[]     = { "clipmenu", NULL };
static const char *calendarcmd[] = { "orage", NULL };
static const char *wificmd[]     = { "urxvt", "-e", "sudo", "wifi-menu", NULL };
static const char *pavucmd[]     = { "pavucontrol", NULL };

static const char *kbcarretcmd[]   = { "bash", "/home/ffm/build/cursor_indicator/cursor_green.sh", NULL };
static const char *kbscrollcmd[]   = { "bash", "/home/ffm/build/cursor_indicator/cursor_red.sh", NULL };
static const char *kbcursorcmd[]   = { "bash", "/home/ffm/build/cursor_indicator/cursor_yellow.sh", NULL };

static const char *upvol[]   = { "amixer", "set", "Master", "5%+",     NULL };
static const char *downvol[] = { "amixer", "set", "Master", "5%-",     NULL };
static const char *mutevol[] = { "pactl", "set-sink-mute", "@DEFAULT_SINK@", "toggle", NULL };

static const char *upbri[]   = { "xbacklight", "+10",     NULL };
static const char *downbri[] = { "xbacklight", "-10",     NULL };
/*
static const char *upbri[]   = { "bash", "/home/ffm/build/mytools/upbri.sh",     NULL };
static const char *downbri[] = { "bash", "/home/ffm/build/mytools/downbri.sh",   NULL };
*/

static const char *screenshotcmd[]  = { "gnome-screenshot", NULL };
static const char *screenshoticmd[] = { "gnome-screenshot", "-i", NULL };

static Key keys[] = {
	/* modifier                       key					   function        argument */
	{ MODKEY,                         XK_p,      	    	   spawn,          {.v = dmenucmd   } },//Programme
	{ MODKEY|ShiftMask,               XK_Return, 	    	   spawn,          {.v = termcmd    } },
	{ ControlMask|Mod1Mask,           XK_f,      	    	   spawn,          {.v = firefoxcmd } },
	{ MODKEY,                         XK_f,      	    	   spawn,          {.v = firefoxcmd } },
	{ ControlMask|Mod1Mask|ShiftMask, XK_f,      	    	   spawn,          {.v = killfirefoxcmd } },
	{ ControlMask|Mod1Mask,           XK_c,      	    	   spawn,          {.v = freecadcmd } },
	{ ControlMask|Mod1Mask,           XK_p,      	    	   spawn,          {.v = pidgincmd  } },
	{ ControlMask|Mod1Mask,           XK_t,      	    	   spawn,          {.v = termcmd    } },
	{ MODKEY,                         XK_v,      	    	   spawn,          {.v = termcmd    } },
	{ ControlMask|Mod1Mask,           XK_r,      	    	   spawn,          {.v = termrgcmd  } },
	{ ControlMask|Mod1Mask,           XK_e,      	    	   spawn,          {.v = nemocmd    } },
	{ ControlMask|Mod1Mask,           XK_l,      	    	   spawn,          {.v = lockcmd    } },
	{ ControlMask|Mod1Mask,           XK_a,      	    	   spawn,          {.v = appcmd     } },
	//{ MODKEY,                         XK_e,      	    	   spawn,          {.v = nemocmd    } },
	//{ MODKEY,                         XK_r,      	    	   spawn,          {.v = clipcmd    } },
	{ MODKEY,                         XF86XK_AudioMute,    	   spawn,          {.v = mutevol    } },//Volume
	{ MODKEY,                         XF86XK_AudioRaiseVolume, spawn,          {.v = upvol      } },
	{ MODKEY,                         XF86XK_AudioLowerVolume, spawn,          {.v = downvol    } },
	{ MODKEY,                         XK_F10,    			   spawn,          {.v = mutevol    } },
	{ MODKEY,                         XK_F11,    			   spawn,          {.v = downvol    } },
	{ MODKEY,                         XK_F12,				   spawn,          {.v = upvol      } },
	{ 0,                              0x1008ff11,			   spawn,          {.v = downvol    } },
	{ 0,                              0x1008ff13,			   spawn,          {.v = upvol      } },
	{ 0,                              XK_Print,  			   spawn,          {.v = screenshotcmd} },
	{ ControlMask|Mod1Mask,           XK_s,      			   spawn,          {.v = screenshoticmd} },
	{ 0,                              0x1008ff02,			   spawn,          {.v = upbri      } },//Brightness
	{ 0,                              0x1008ff03,			   spawn,          {.v = downbri    } },
	{ ControlMask|Mod1Mask|ShiftMask, XK_o,      			   spawn,          {.v = poweroffcmd} },//Systemcommands
	{ ControlMask|Mod1Mask|ShiftMask, XK_r,      			   spawn,          {.v = rebootcmd  } },
	{ ShiftMask|Mod1Mask|ControlMask, XK_s,      			   spawn,          {.v = suspendcmd } },
	{ 0,                              XK_F16,      	           spawn,          {.v = killcursorcmd } },
	{ 0,                              XK_F17,      	           spawn,          {.v = killcursorcmd } },
	{ 0,                              XK_F18,      	           spawn,          {.v = killcursorcmd } },
	{ 0,                              XK_F16,      	           spawn,          {.v = kbcarretcmd } },
	{ 0,                              XK_F17,      	           spawn,          {.v = kbscrollcmd } },
	{ 0,                              XK_F18,      	           spawn,          {.v = kbcursorcmd } },
	{ MODKEY,                         XK_b,      			   togglebar,      {0} },
	{ MODKEY,                         XK_n,      			   focusstack,     {.i = +1 } },
	{ MODKEY,                         XK_e,      			   focusstack,     {.i = -1 } },
	{ MODKEY,                         XK_Up,      			   focusstack,     {.i = -1 } },
	{ MODKEY,                         XK_Down,     			   focusstack,     {.i = +1 } },
	{ MODKEY,                         XK_minus,   			   incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,               XK_minus,    			   incnmaster,     {.i = -1 } },
	{ MODKEY,                         XK_h,      			   setmfact,       {.f = -0.05} },
	{ MODKEY,                         XK_i,      			   setmfact,       {.f = +0.05} },
	{ MODKEY,                         XK_Left,      		   setmfact,       {.f = -0.01} },
	{ MODKEY,                         XK_Right,      		   setmfact,       {.f = +0.01} },
 	{ MODKEY|ShiftMask,               0xf6,      			   setmfact,       {.f = 1.55} }, /*oe reset*/
	{ MODKEY,                         XK_Return, 			   zoom,           {0} },
	{ MODKEY,                         XK_Tab,    			   view,           {0} },
	{ MODKEY|ShiftMask,               XK_c,      			   killclient,     {0} },
	{ MODKEY,                         XK_q,      			   killclient,     {0} },
	{ Mod1Mask,                       XK_F4,     			   killclient,     {0} },
	{ MODKEY,                         XK_comma,    			   setlayout,      {.v = &layouts[0]} },/* tile */
	{ MODKEY,                         XK_period,   			   setlayout,      {.v = &layouts[1]} },/* floating */
	{ MODKEY,                         XK_m,      			   setlayout,      {.v = &layouts[2]} },/* monocle */
	//{ MODKEY,                         XK_u,      			   setlayout,      {.v = &layouts[3]} },/* centeredmaster */
	{ MODKEY,                         XK_F13,    			   setlayout,      {.v = &layouts[4]} },/* centeredfloatingmaster (Disabled) */
	{ MODKEY,                         XK_space,  			   setlayout,      {0} },
	{ MODKEY|ShiftMask,               XK_space,  			   togglefloating, {0} },
	{ MODKEY|ShiftMask,               XK_semicolon,      	   view,           {.ui = ~0 } },
	{ MODKEY,						  XK_semicolon,			   tag,            {.ui = ~0 } },
	{ MODKEY,						  XK_colon,			       tag,            {.ui = ~0 } },
	{ MODKEY|ShiftMask,				  XK_colon,			       tag,            {.ui = ~0 } },
	{ MODKEY,                         XK_o,  			       focusmon,       {.i = -1 } },
	{ MODKEY,                         XK_backslash,			   focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,               XK_o,    			       tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,               XK_backslash,			   tagmon,         {.i = +1 } },
//	{ MODKEY,                         XK_Q, 	    		   movemouse,      {0} }, // move window
//	{ MODKEY|Mod1Mask|ControlMask,    XK_W, 	    		   togglefloating, {0} },
//	{ MODKEY|Mod1Mask|ControlMask,    XK_P, 	    		   resizemouse,    {0} }, // resize window
	TAGKEYS(                          XK_a,      			                   0)
	TAGKEYS(                          XK_r,      			                   1)
	TAGKEYS(                          XK_s,      			                   2)
	TAGKEYS(                          XK_t,      			                   3)
	TAGKEYS(                          XK_g,      			                   4)
	TAGKEYS(                          XK_j,      			                   5)
	TAGKEYS(                          XK_l,      			                   6)
	TAGKEYS(                          XK_u,      			                   7)
	TAGKEYS(                          XK_y,      			                   8)
	{ MODKEY|ShiftMask,               XK_i,      			   quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */

/* laptop configuration */
int Text1 = 130; // Date
int Text2 = 160; // Sound
int Text3 = 220; // Sys
// Text4 is everything left of Text3

/* tower configuration
int Text1 = 160; // Date
int Text2 = 207; // Sound
int Text3 = 288; // Sys
*/

static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText1,       0,              Button1,        spawn,          {.v = calendarcmd } },
	{ ClkStatusText2,       0,              Button1,        spawn,          {.v = pavucmd } },
	{ ClkStatusText2,       0,              Button4,        spawn,          {.v = upvol } },
	{ ClkStatusText2,       0,              Button5,        spawn,          {.v = downvol } },
	{ ClkStatusText3,       0,              Button1,        spawn,          {.v = termcmd } },
	{ ClkStatusText3,       0,              Button4,        spawn,          {.v = upbri } },
	{ ClkStatusText3,       0,              Button5,        spawn,          {.v = downbri } },
	{ ClkStatusText4,       0,              Button1,        spawn,          {.v = wificmd } },
	{ ClkClientWin,			MODKEY,         Button4,        setmfact,       {.f = -0.01 } },
	{ ClkClientWin,			MODKEY,         Button5,        setmfact,       {.f = +0.01 } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
